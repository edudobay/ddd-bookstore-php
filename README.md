# DDD Bookstore (PHP version)

This is an example project using DDD (Domain-Driven Design) principles and techniques to create a backoffice management system for a fictitious bookstore.

## Attempt 1: Active Record pattern (Eloquent ORM)

The Active Record pattern goes against most of the DDD principles if applied to the domain objects. I'll see how far I can go using Active Records as Data Transfer Objects, trying to keep the persistence logic encapsulated away from the domain objects.

## Setup

### Tests

* Run `composer test` to run the test suite.
* With `composer test-coverage` you will also get the coverage report.
