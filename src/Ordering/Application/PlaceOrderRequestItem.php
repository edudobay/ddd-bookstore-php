<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Application;

class PlaceOrderRequestItem
{
    public string $title;
    public int $quantity;
}
