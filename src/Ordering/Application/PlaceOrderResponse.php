<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Application;

class PlaceOrderResponse
{
    public string $placedOrderId;

    public function __construct(string $placedOrderId)
    {
        $this->placedOrderId = $placedOrderId;
    }
}
