<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Application;

use Bookstore\Ordering\Model\Order;
use Bookstore\Ordering\Model\OrderId;
use Bookstore\Ordering\Model\OrderRepository;

class RetrieveOrderService
{
    /**
     * @var OrderRepository
     */
    private OrderRepository $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getOrderById(string $placedOrderId): Order
    {
        $order = $this->orderRepository->findOrderById(new OrderId($placedOrderId));
        if ($order === null) {
            throw new \RuntimeException('Order not found');
        }
        return $order;
    }
}
