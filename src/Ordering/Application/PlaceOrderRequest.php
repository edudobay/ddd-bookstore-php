<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Application;

class PlaceOrderRequest
{
    public string $customerName;

    public string $customerEmail;

    /** @var PlaceOrderRequestItem[] */
    public array $items;
}
