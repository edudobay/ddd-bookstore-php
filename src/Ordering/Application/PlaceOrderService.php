<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Application;

use Bookstore\Common\DateTime\Clock;
use Bookstore\Ordering\Model\Book;
use Bookstore\Ordering\Model\Customer;
use Bookstore\Ordering\Model\Order;
use Bookstore\Ordering\Model\OrderItem;
use Bookstore\Ordering\Model\OrderRepository;

class PlaceOrderService
{
    private Clock $clock;
    private OrderRepository $orderRepository;

    public function __construct(Clock $clock, OrderRepository $orderRepository)
    {
        $this->clock = $clock;
        $this->orderRepository = $orderRepository;
    }

    public function placeOrder(PlaceOrderRequest $request): PlaceOrderResponse
    {
        $orderId = $this->orderRepository->generateId();

        $order = Order::placeOrder(
            $orderId,
            new Customer($request->customerName, $request->customerEmail),
            array_map(fn (PlaceOrderRequestItem $item) => new OrderItem(
                new Book($item->title),
                $item->quantity
            ), $request->items),
            $this->clock->now()
        );

        $this->orderRepository->add($order);

        return new PlaceOrderResponse($order->id->id);
    }
}
