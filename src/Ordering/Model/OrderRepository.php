<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Model;

interface OrderRepository
{
    public function findOrderById(OrderId $orderId): ?Order;

    public function generateId(): OrderId;

    public function add(Order $order);
}
