<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Model;

class Book
{
    public string $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }
}
