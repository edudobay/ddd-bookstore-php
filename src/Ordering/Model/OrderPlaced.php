<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Model;

use DateTimeImmutable;

class OrderPlaced implements OrderEvent
{
    public OrderId $id;

    public Customer $customer;

    /** @var OrderItem[] */
    public array $items;

    public DateTimeImmutable $placedAt;

    /**
     * @param OrderId $id
     * @param Customer $customer
     * @param OrderItem[] $items
     * @param DateTimeImmutable $placedAt
     */
    public function __construct(OrderId $id, Customer $customer, $items, DateTimeImmutable $placedAt)
    {
        $this->id = $id;
        $this->customer = $customer;
        $this->items = $items;
        $this->placedAt = $placedAt;
    }
}
