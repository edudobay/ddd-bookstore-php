<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Model;

use Webmozart\Assert\Assert;

class Customer
{
    public string $name;
    public string $email;

    public function __construct(string $name, string $email)
    {
        Assert::email($email);

        $this->name = $name;
        $this->email = $email;
    }
}
