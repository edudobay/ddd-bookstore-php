<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Model;

class OrderId
{
    public string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }
}
