<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Model;

use Webmozart\Assert\Assert;

class OrderItem
{
    public Book $book;
    public int $quantity;

    public function __construct(Book $book, int $quantity)
    {
        Assert::greaterThanEq($quantity, 1);

        $this->book = $book;
        $this->quantity = $quantity;
    }
}
