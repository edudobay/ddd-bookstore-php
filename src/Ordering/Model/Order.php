<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Model;

use DateTimeImmutable;
use Webmozart\Assert\Assert;

class Order
{
    public OrderId $id;

    public Customer $customer;

    /** @var OrderItem[] */
    private array $items;

    public DateTimeImmutable $placedAt;

    /** @var OrderEvent[] */
    private array $recordedEvents;

    public function __construct(OrderId $id, Customer $customer, array $items, DateTimeImmutable $placedAt, array $recordedEvents = [])
    {
        $this->id = $id;
        $this->customer = $customer;
        $this->items = $items;
        $this->placedAt = $placedAt;
        $this->recordedEvents = $recordedEvents;
    }

    /**
     * @param OrderId $id
     * @param Customer $customer
     * @param OrderItem[] $items
     * @param DateTimeImmutable $placedAt
     * @return Order
     */
    public static function placeOrder(OrderId $id, Customer $customer, array $items, DateTimeImmutable $placedAt): Order
    {
        Assert::allIsInstanceOf($items, OrderItem::class);

        $event = new OrderPlaced($id, $customer, $items, $placedAt);

        return new Order($id, $customer, $items, $placedAt, [$event]);
    }

    /**
     * @return OrderItem[]
     */
    public function items(): array
    {
        return $this->items;
    }

    public function recordedEvents(): array
    {
        return $this->recordedEvents;
    }
}
