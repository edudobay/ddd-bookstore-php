<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Infrastructure;

use Bookstore\Common\Persistence\BaseModel;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property int $id
 * @property string $customer_name
 * @property string $customer_email
 * @property CarbonImmutable $placed_at
 *
 * @property Collection|OrderItemRecord[] $items
 */
class OrderRecord extends BaseModel
{
    protected $table = 'orders';

    protected $dates = [
        'placed_at',
    ];

    /**
     * @codeCoverageIgnore
     */
    public function items()
    {
        return $this->hasMany(OrderItemRecord::class, 'order_id');
    }
}
