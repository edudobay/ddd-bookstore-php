<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Infrastructure;

use Bookstore\Common\Persistence\BaseModel;

/**
 * @property int $id
 * @property int $order_id
 * @property string $book_title
 * @property int $quantity
 *
 * @property OrderRecord $order
 */
class OrderItemRecord extends BaseModel
{
    protected $table = 'order_items';

    /**
     * @codeCoverageIgnore
     */
    public function order()
    {
        return $this->belongsTo(OrderRecord::class, 'order_id');
    }
}
