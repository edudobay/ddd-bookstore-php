<?php
declare(strict_types=1);

namespace Bookstore\Ordering\Infrastructure;

use Bookstore\Ordering\Model\Book;
use Bookstore\Ordering\Model\Customer;
use Bookstore\Ordering\Model\Order;
use Bookstore\Ordering\Model\OrderId;
use Bookstore\Ordering\Model\OrderItem;
use Bookstore\Ordering\Model\OrderRepository;
use Illuminate\Database\Connection;
use Illuminate\Support\Collection;

class EloquentOrderRepository implements OrderRepository
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function findOrderById(OrderId $orderId): ?Order
    {
        $record = OrderRecord::on($this->connection->getName())->with('items')->find($orderId->id);
        if ($record === null) {
            return null;
        }

        return $this->hydrateOrder($record);
    }

    private function hydrateOrder(OrderRecord $record): Order
    {
        $items = $record->items->map(fn (OrderItemRecord $item) => new OrderItem(
            new Book($item->book_title),
            $item->quantity
        ));

        return new Order(
            new OrderId((string) $record->id),
            new Customer($record->customer_name, $record->customer_email),
            $items->toArray(),
            $record->placed_at->toDateTimeImmutable()
        );
    }

    public function generateId(): OrderId
    {
        $id = $this->connection->query()
            ->selectRaw("nextval('orders_id_seq') AS id")
            ->first()->id;

        return new OrderId((string) $id);
    }

    public function add(Order $order)
    {
        $record = new OrderRecord();
        $record->id = $order->id->id;
        $record->customer_name = $order->customer->name;
        $record->customer_email = $order->customer->email;
        $record->placed_at = $order->placedAt;
        $record->save();

        $record->items()->saveMany(Collection::make($order->items())->map(fn (OrderItem $item) => new OrderItemRecord([
            'book_title' => $item->book->title,
            'quantity' => $item->quantity,
        ])));
    }
}
