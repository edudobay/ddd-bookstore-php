<?php
declare(strict_types=1);

namespace Bookstore\Common\DateTime;

use DateTimeImmutable;

interface Clock
{
    public function now(): DateTimeImmutable;
}
