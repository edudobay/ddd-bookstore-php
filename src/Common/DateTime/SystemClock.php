<?php
declare(strict_types=1);

namespace Bookstore\Common\DateTime;

use DateTimeImmutable;

class SystemClock implements Clock
{
    public function now(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}
