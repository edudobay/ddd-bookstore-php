<?php
declare(strict_types=1);

namespace Bookstore\Common\Persistence;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public $dateFormat = 'Y-m-d H:i:s.uP';

    public $timestamps = false;

    protected $guarded = [];
}
