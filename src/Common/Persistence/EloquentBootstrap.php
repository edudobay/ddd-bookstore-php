<?php
declare(strict_types=1);

namespace Bookstore\Common\Persistence;

use Carbon\CarbonImmutable;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Support\Facades\Date;

class EloquentBootstrap
{
    public static function boot(string $environment)
    {
        $capsule = new Manager();

        $defaults = [
            'driver' => 'pgsql',
            'charset' => 'utf8',
            'prefix' => '',
        ];

        if ($environment !== 'testing') {
            throw new \InvalidArgumentException("Only testing environment can be activated currently");
        }

        $testingConfig = [
            'host' => 'localhost',
            'port' => 5432,
            'database' => 'bookstore_testing',
            'username' => 'postgres',
            'password' => 'password',
        ];

        $capsule->addConnection(array_merge($testingConfig, $defaults));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        Date::use(CarbonImmutable::class);
    }
}
