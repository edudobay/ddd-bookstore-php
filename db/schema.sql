create table orders
(
    id bigserial primary key,
    placed_at timestamptz not null,
    customer_name varchar not null,
    customer_email varchar not null
);

create table order_items
(
    id bigserial primary key,
    order_id bigint references orders(id) not null,
    book_title varchar not null,
    quantity int not null
);
