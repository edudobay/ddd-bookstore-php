<?php
declare(strict_types=1);

namespace BookstoreTests\Ordering\Fixtures;

use Bookstore\Ordering\Model\Order;
use Bookstore\Ordering\Model\OrderId;
use Bookstore\Ordering\Model\OrderRepository;

class InMemoryOrderRepository implements OrderRepository
{
    private array $orders;
    private int $lastId;

    public function __construct()
    {
        $this->orders = [];
        $this->lastId = 0;
    }

    public function findOrderById(OrderId $orderId): ?Order
    {
        return $this->orders[$orderId->id] ?? null;
    }

    public function generateId(): OrderId
    {
        return new OrderId((string) ++$this->lastId);
    }

    public function add(Order $order)
    {
        $this->orders[$order->id->id] = $order;
    }
}
