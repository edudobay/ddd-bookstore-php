<?php
declare(strict_types=1);

namespace BookstoreTests\Ordering;

use Bookstore\Common\DateTime\SystemClock;
use Bookstore\Common\Serialization\SerializerFactory;
use Bookstore\Ordering\Application\PlaceOrderRequest;
use Bookstore\Ordering\Application\PlaceOrderService;
use Bookstore\Ordering\Application\RetrieveOrderService;
use BookstoreTests\Common\Fixtures\FixedClock;
use BookstoreTests\Ordering\Fixtures\InMemoryOrderRepository;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class PlaceOrderTest extends TestCase
{
    public function testPlaceOrderWithSingleBook()
    {
        $placeOrderService = new PlaceOrderService(new SystemClock(), new InMemoryOrderRepository());

        $request = $this->makePlaceOrderRequest([
            'customerName' => 'William Blake',
            'customerEmail' => 'wblake@example.com',
            'items' => [
                ['title' => 'The Great Gatsby', 'quantity' => 1],
            ],
        ]);

        $response = $placeOrderService->placeOrder($request);

        self::assertIsString($response->placedOrderId);
    }

    private function makePlaceOrderRequest(array $data): PlaceOrderRequest
    {
        $serializer = (new SerializerFactory())->createSerializer();
        return $serializer->denormalize($data, PlaceOrderRequest::class);
    }

    public function testRetrievePlacedOrder()
    {
        $repository = new InMemoryOrderRepository();
        $placeOrderService = new PlaceOrderService(new FixedClock(new DateTimeImmutable('2020-02-02T13:47:01Z')), $repository);
        $retrieveOrderService = new RetrieveOrderService($repository);

        $fakeNow = new DateTimeImmutable('2020-02-02T13:47:01Z');

        $request = $this->makePlaceOrderRequest([
            'customerName' => 'William Blake',
            'customerEmail' => 'wblake@example.com',
            'items' => [
                ['title' => 'The Great Gatsby', 'quantity' => 1],
            ],
        ]);
        $response = $placeOrderService->placeOrder($request);

        $order = $retrieveOrderService->getOrderById($response->placedOrderId);

        self::assertCount(1, $order->items());
        self::assertEquals($fakeNow, $order->placedAt);
    }
}
