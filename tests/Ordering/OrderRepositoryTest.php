<?php
declare(strict_types=1);

namespace BookstoreTests\Ordering;

use Bookstore\Common\Persistence\EloquentBootstrap;
use Bookstore\Ordering\Infrastructure\EloquentOrderRepository;
use Bookstore\Ordering\Infrastructure\OrderItemRecord;
use Bookstore\Ordering\Infrastructure\OrderRecord;
use Bookstore\Ordering\Model\Book;
use Bookstore\Ordering\Model\Customer;
use Bookstore\Ordering\Model\Order;
use Bookstore\Ordering\Model\OrderId;
use Bookstore\Ordering\Model\OrderItem;
use Bookstore\Ordering\Model\OrderRepository;
use BookstoreTests\Common\Persistence\PostgreSQLSequenceUtils;
use BookstoreTests\Ordering\Fixtures\InMemoryOrderRepository;
use Illuminate\Database\Capsule\Manager;
use PHPUnit\Framework\TestCase;

class OrderRepositoryTest extends TestCase
{
    protected function setUp(): void
    {
        EloquentBootstrap::boot('testing');
    }

    protected function tearDown(): void
    {
        OrderItemRecord::query()->delete();
        OrderRecord::query()->delete();
        PostgreSQLSequenceUtils::createDefault()->restart('orders_id_seq');
    }

    /**
     * @param string $implementationType
     * @dataProvider provideRepositoryImplementationTypes
     */
    public function testGenerateIdWorks(string $implementationType)
    {
        $repository = $this->getRepositoryInstance($implementationType);

        $id = $repository->generateId();
        self::assertInstanceOf(OrderId::class, $id);
    }

    /**
     * @param string $implementationType
     * @dataProvider provideRepositoryImplementationTypes
     */
    public function testFindNonExistingOrderReturnsNull(string $implementationType)
    {
        $repository = $this->getRepositoryInstance($implementationType);

        self::assertNull($repository->findOrderById(new OrderId('99999')));
    }

    /**
     * @param string $implementationType
     * @dataProvider provideRepositoryImplementationTypes
     */
    public function testPersistTwoOrders(string $implementationType)
    {
        $repository = $this->getRepositoryInstance($implementationType);

        $firstId = $repository->generateId();
        $secondId = $repository->generateId();

        $order1 = Order::placeOrder(
            $firstId,
            new Customer('Walt Disney', 'walt@disney.com'),
            [
                new OrderItem(new Book('The Great Gatsby'), 2),
                new OrderItem(new Book('Crime and Punishment'), 1),
                new OrderItem(new Book('Pride and Prejudice'), 5),
            ],
            new \DateTimeImmutable());

        $order2 = Order::placeOrder(
            $secondId,
            new Customer('Isaac Newton', 'isaac.newton@example.com'),
            [
                new OrderItem(new Book('Philosophiæ Naturalis Principia Mathematica'), 10),
            ],
            new \DateTimeImmutable());

        $repository->add($order1);
        $repository->add($order2);

        $retrievedOrder1 = $repository->findOrderById($firstId);
        $retrievedOrder2 = $repository->findOrderById($secondId);

        self::assertInstanceOf(Order::class, $retrievedOrder1);
        self::assertInstanceOf(Order::class, $retrievedOrder2);

        self::assertEquals('Walt Disney', $retrievedOrder1->customer->name);
        self::assertEquals('Isaac Newton', $retrievedOrder2->customer->name);

        self::assertCount(3, $retrievedOrder1->items());
        self::assertCount(1, $retrievedOrder2->items());
    }

    public function provideRepositoryImplementationTypes(): array
    {
        return [
            'in-memory' => ['in-memory'],
            'eloquent' => ['eloquent'],
        ];
    }

    public function getRepositoryInstance(string $type): OrderRepository
    {
        switch ($type) {
            case 'in-memory': return new InMemoryOrderRepository();
            case 'eloquent': return new EloquentOrderRepository(Manager::connection());
            default: throw new \InvalidArgumentException("Invalid repository type: $type");
        }
    }
}
