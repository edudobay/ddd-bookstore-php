<?php
declare(strict_types=1);

namespace BookstoreTests\Common\Persistence;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Connection;

class PostgreSQLSequenceUtils
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public static function createDefault(): self
    {
        return new self(Manager::connection());
    }

    public function restart(string $sequenceName)
    {
        $escapedSequenceName = $this->connection->getQueryGrammar()->wrap($sequenceName);
        $this->connection->statement("ALTER SEQUENCE $escapedSequenceName RESTART");
    }

}
