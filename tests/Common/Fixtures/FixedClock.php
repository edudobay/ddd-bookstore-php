<?php
declare(strict_types=1);

namespace BookstoreTests\Common\Fixtures;

use Bookstore\Common\DateTime\Clock;
use DateTimeImmutable;

class FixedClock implements Clock
{
    private DateTimeImmutable $fixedDateTime;

    public function __construct(DateTimeImmutable $fixedDateTime)
    {
        $this->fixedDateTime = $fixedDateTime;
    }

    public function now(): DateTimeImmutable
    {
        return $this->fixedDateTime;
    }
}
